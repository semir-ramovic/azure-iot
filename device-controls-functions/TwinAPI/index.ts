import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import TwinManager from "./twinManager";

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
  let twinManager: TwinManager;

  try {
    twinManager = new TwinManager();
  } catch (e) {
    context.log(e);
    context.res = {
      status: 500,
      body: "Internal Server Error"
    };
    return;
  }

  if (!req.query && !req.query.id) {
    context.res.status(400).send("Please pass a device id in the request query");
    return;
  }

  let device_id = req.params.id;
  context.log(req);

  if (req.method == "GET") {
    await twinManager.getTwin(context, device_id);
  } 
  else if (req.method == "POST") {
    if (!req.query.patch || !req.query.etag) {
      context.res.status(400).send("Please pass a patch and etag in the request query");
      return;
    }
    await twinManager.updateTwin(context, device_id, req.query.patch, req.query.etag);
  }
};

export default httpTrigger;