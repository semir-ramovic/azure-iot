import { Registry } from "azure-iothub";
import env_config from "../env_config";

export class TwinManager {

  registry: Registry;

  constructor() {
    if (!env_config.connection_string) {
      throw new Error("Missing IOTHUB_CONNECTION_STRING string in process.env");
    }

    try {
      this.registry = Registry.fromConnectionString(env_config.connection_string);
    } catch (e) {
      throw new Error("Failed to establish a connection with device registry");
    }
  }

  async getTwin(context: any, device_id: string) {
    try {
      const result = await this.registry.getTwin(device_id);
      context.res.status(200).json(result.responseBody.toJSON());
    } catch (e) {
      context.log(e);
      context.res.status(404).send(`Device ${device_id} not found`);
    }
  }

  async updateTwin(context: any, device_id: string, patch: any, etag: string) {
    try {
      const result = await this.registry.updateTwin(device_id, patch, etag);
      context.res.status(200).json(result.responseBody.toJSON());
    } catch (e) {
      context.log(e);
      context.res.status(404).send(`Device ${device_id} not found`);
    }
  }
}

export default TwinManager;