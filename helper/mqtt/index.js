var clientFromConnectionString = require('azure-iot-device-mqtt').clientFromConnectionString;
var Message = require('azure-iot-device').Message;

var connectionString = 'HostName=easelink-iothub.azure-devices.net;DeviceId=test-mcc;SharedAccessKey=MpO6qaVA7OgGnuG/J2S9w/k+LM0cJ0x1u5RrJighoUQ=';

var client = clientFromConnectionString(connectionString);

var connectCallback = function (err) {
  if (err) {
    console.error('Could not connect: ' + err);
  } else {
    console.log('Client connected');

    return
    // const logMsg = msgpack.encode([1, 'Connector', 'Test ' + time, Date.now(), 41])

    const uid = Math.floor(Math.random() * 2e32).toString();
    const messages = [
      {
        'type': 'event',
        'timestamp': Date.now(),
        'content': {
          'name': 'START_SESSION',
          'parameters': [uid]
        }
      },
      {
        'type': 'message',
        'timestamp': Date.now(),
        'session_uid': uid,
        'content': {
          'type': 0,
          'name': 'AIR_BLOWER',
          'value': 'Failed to start due to low power.'
        }
      },
      {
        'type': 'measurement',
        'timestamp': Date.now(),
        'session_uid': uid,
        'content': {
          'name': 'TEMPERATURE_01',
          'value': 23
        }
      },
      {
        'type': 'measurement',
        'timestamp': Date.now(),
        'session_uid': uid,
        'content': {
          'name': 'TEMPERATURE_01',
          'value': 23
        }
      },
      {
        'type': 'measurement',
        'timestamp': Date.now(),
        'session_uid': uid,
        'content': {
          'name': 'TEMPERATURE_01',
          'value': 27
        }
      },
      {
        'type': 'measurement',
        'timestamp': Date.now(),
        'session_uid': uid,
        'content': {
          'name': 'TEMPERATURE_01',
          'value': 33
        }
      },
      {
        'type': 'measurement',
        'timestamp': Date.now(),
        'session_uid': uid,
        'content': {
          'name': 'TEMPERATURE_01',
          'value': 56
        }
      }
    ];

    for (let messageString of messages) {
      var message = new Message(JSON.stringify(messageString));

      client.sendEvent(message, function (err) {
        if (err) console.log(err.toString());
        console.log('Message sent.');
      });

      client.on('message', function (msg) {
        console.log(msg);
        client.complete(msg, function () {
          console.log('completed');
        });
      });
    }
  }
};

client.open(connectCallback);
