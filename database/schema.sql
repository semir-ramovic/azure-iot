-- device
create table device (
  id varchar(32) constraint device_pk primary key, 
  name varchar(64), 
  type integer default 0 not null,
  last_online timestamp, 
  online boolean default false, 
  connection_string varchar(256), 
  icon varchar(256)
);

alter table 
  device owner to easelinkiotdb;

create unique index device_id_uindex on device (id);

-- event
create table event (
  id serial not null constraint event_pk primary key,
  device_id varchar(32) constraint event_device_id_fk references device,
  timestamp timestamp not null,
  timestamp_received timestamp,
  name varchar(64) not null
);

alter table 
  event owner to easelinkiotdb;

create unique index event_id_uindex on event (id);

-- parameter
create table event_parameter (
  id serial not null constraint event_parameter_pk primary key,
  event_id integer not null constraint event_parameter_event_id_fk references event,
  value varchar(32) not null
);

alter table 
  event_parameter owner to easelinkiotdb;

create unique index event_parameter_id_uindex on event_parameter (id);

-- message
create table message (
  id serial not null constraint message_pk primary key,
  device_id varchar(32) constraint message_device_id_fk references device,
  timestamp timestamp not null,
  timestamp_received timestamp,
  type integer default 0,
  name varchar(64),
  value varchar(256) not null,
  session_uid numeric
);

alter table 
  message owner to easelinkiotdb;

create unique index message_id_uindex on message (id);

-- measurement
create table measurement (
  id serial not null constraint measurement_pk primary key, 
  device_id varchar(32) null constraint measurement_device_id_fk references device, 
  timestamp timestamp not null,
  timestamp_received timestamp, 
  name varchar(64),
  value double precision, 
  session_uid numeric
);

alter table 
  measurement owner to easelinkiotdb;

create unique index measurement_id_uindex on measurement (id);