import { AzureFunction, Context } from "@azure/functions"
import { Pool } from "pg";

const eventGridTrigger: AzureFunction = async function (context: Context, eventGridEvent: any): Promise<void> {
  context.log(typeof eventGridEvent);
  context.log(eventGridEvent);
  
  const deviceId: string = eventGridEvent.data.deviceId;
  const config = {
    host: process.env['POSTGRES_HOST_V2'],
    user: process.env['POSTGRES_USER_V2'],
    password: process.env['POSTGRES_PASSWORD_V2'],
    database: 'postgres',
    port: 5432,
    ssl: true
  };

  try {
    const pool = new Pool(config);
    const client = await pool.connect();
    
    if (eventGridEvent.eventType == "Microsoft.Devices.DeviceConnected") {
      const values = [deviceId];
      let query = 'select status from device where id = $1;';
      let device = (await client.query(query, values)).rows[0];

      if (device.status == 0) {
        query = 'update device set status = 1 where id = $1;';
        client.query(query, values);
      }
    }
    else if (eventGridEvent.eventType == "Microsoft.Devices.DeviceDisconnected") {
      const values = [deviceId];
      const query = 'update device set status = 0 where id = $1;';
      client.query(query, values);
    }
    else if (eventGridEvent.eventType == "Microsoft.Devices.DeviceCreated") {
      const type = deviceId.toLowerCase().includes("mcp") ? 0 : deviceId.toLowerCase().includes("mcc") ? 1 : 2;
      const values = [deviceId, type];
      const query = 'insert into device(id, name, type) values($1,$1,$2);';
      client.query(query, values);
    }
    else if (eventGridEvent.eventType == "Microsoft.Devices.DeviceDeleted") {
      const values = [deviceId];
      const query = 'delete from device where id = $1;';
      client.query(query, values);
    }
    client.release();
  } catch (err) {
    context.log(err.message);
    context.log(err.stack);
  } finally {
    context.done();
  }
};

export default eventGridTrigger;
