import { AzureFunction, Context } from "@azure/functions"
import { Pool } from "pg";

enum MessageType {
  event = 0,
  log = 1,
  measurement = 2,
  config = 3
};

const IoTHubTrigger: AzureFunction = async function (context: Context, IoTHubMessages: any[]): Promise<void> {
  const config = {
    host: process.env['POSTGRES_HOST_V2'],
    user: process.env['POSTGRES_USER_V2'],
    password: process.env['POSTGRES_PASSWORD_V2'],
    database: 'postgres',
    port: 5432,
    ssl: true
  };

  try {
    const pool = new Pool(config);
    const client = await pool.connect();

    for(let i = 0; i < IoTHubMessages.length; ++i) {
      const message = IoTHubMessages[i];
    
      const type: number = message.type;
      let timestamp = new Date(message.timestamp);
      const timestampReceived = new Date(Date.now());
      const deviceId: string = context.bindingData.systemPropertiesArray[i]['iothub-connection-device-id'];

      // Update online and last_online
      const device_values = [timestampReceived, deviceId];
      const device_query = 'update device set last_online = $1 where id = $2;';
      client.query(device_query, device_values);
      
      const session_uid: string | null = ('session_uid' in message) ? message.session_uid : null;

      if (type == MessageType.log) {
        const message_type: number = message.content.type;
        const name: string = message.content.name;
        const value: string = message.content.value;

        const values = [deviceId, timestamp, timestampReceived, message_type, name, value, session_uid];
        const query = 'insert into message(device_id, timestamp, timestamp_received, type, name, value, session_uid) values($1, $2, $3, $4, $5, $6, $7);';
        client.query(query, values);
      } 
      else if (type == MessageType.measurement) {
        const name: string = message.content.name;
        const value: number | number[] = message.content.value;

        if (Array.isArray(value)) {
          const timestamps: Date[] = message.content.timestamps.map((i: any) => new Date(i));

          const values = [deviceId, timestampReceived, name, session_uid, ...timestamps, ...value];
          let query = 'insert into measurement(device_id, timestamp_received, name, session_uid, timestamp, value) values ';
          for (let i = 0; i < value.length; i++) {
            query += '($1, $2, $3, $4, $' + (i+5).toString() + ', $' + (i+5+timestamps.length).toString() + '),';
          }
          query = query.substr(0, query.length - 1) + ';';
          client.query(query, values);
        } else {
          const values = [deviceId, timestamp, timestampReceived, name, value, session_uid];
          const query = 'insert into measurement(device_id, timestamp, timestamp_received, name, value, session_uid) values($1, $2, $3, $4, $5, $6);';
          client.query(query, values);
        }
      } else if (type == MessageType.event) {
        const name: string = message.content.name;
        const parameters: Array<string> = message.content.parameters;

        if (name == "OFFLINE") {
          timestamp = new Date(Date.now());
        }
        
        const values = [deviceId, timestamp, timestampReceived, name, session_uid];
        const query = 'insert into event(device_id, timestamp, timestamp_received, name, session_uid) values($1, $2, $3, $4, $5) RETURNING id;';
        const { rows } = await client.query(query, values);
        const eventId = rows[0].id;
        
        await Promise.all(
            parameters.map((parameter) => {
                const param_values = [eventId, parameter];
                const param_query = 'insert into event_parameter(event_id, value) values($1, $2);';
                client.query(param_query, param_values);
            })
        );

        const device_status = (name == "START_SESSION") ? 2 : (name == "STOP_SESSION") ? 1 : -1;
        if (device_status == 2 || device_status == 1) {
          const device_values = [device_status, deviceId];
          const device_query = 'update device set status = $1 where id = $2;';
          client.query(device_query, device_values);
        }


      } else if (type == MessageType.config) {
        const name: string = message.content.name;
        const value: string = message.content.value;
        
        const values = [deviceId, name, value];
        const query = 'insert into configuration(device_id, name, value) values($1, $2, $3) on conflict on constraint configuration_name_device_id_key do update set name = $2, value = $3;';
        client.query(query, values);
      }
    }

    client.release();
  } catch (err) {
    context.log(err.message);
    context.log(err.stack);
  } finally {
    context.done();
  }
};

export default IoTHubTrigger;
